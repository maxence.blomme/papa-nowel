# Papa Nowel

Tirage de Père Noël Secret et envoi des destinataires par mail

## Configuration

L'intégralité de la configuration se fait dans un fichier `data.yml` à mettre
dans le dossier d'exécution. Un exemple de ce fichier est fourni en le fichier
`data_sample.yml`.

2 objets y sont nécessaires :
- email
- people

2 objets y sont optionnels :
- smtp
- local

### Email

Cet objet donne le contenu des emails qui seront envoyés aux participants. Les
champs nécessaires sont les suivants :
```yaml
email:
  subject: # sujet de l'e-mail
  content: # contenu de l'e-mail
```
Sujet comme contenu peuvent faire figurer les noms de la personne qui reçoit l'e-mail
(qui doit offrir le cadeau) via le placeholder `{name}` et de la personne qui recevra
le cadeau via le placeholder `{dest}`.

### People

Cet objet donne la liste des participants du Père Noël Secret ainsi que leurs
adresses mail et contraintes. La liste des participants se présente ainsi :
```yaml
people:
  Alice: [...]
  Bob:   [...]
```
*Attention :* Le nom de chaque champ de `people` sera le nom donné à chaque personne lors
du tirage et de l'envoi du mail

Chaque personne est un objet ayant un champ obligatoire `email` qui est son adresse mail,
et un champ optionnel `cantget` qui est une liste de personnes à qui cette personne ne doit
pas offrir de cadeau.

Ainsi, une liste de personnes se présente ainsi :
```yaml
people:
  Alice:
    email: alice@example.com
  Bob:
    email: bob@example.com
    cantget:
      - alice
      - [...]   # si Bob a d'autres personnes à qui il ne peut pas faire de cadeau
```

### SMTP (envoi du tirage par mail)

Cet objet donne les informations de connexion au serveur d'envoi de mail. Les
champs nécessaires sont les suivants :
```yaml
smtp:
  server: # adresse du serveur SMTP auquel se connecter
  port:   # port de connexion au serveur via SSL (laisser à 465 par défaut)
  login:  # login de connexion au serveur SMTP
```
À ces champs peut s'ajouter le champ `password` du mot de passe de connexion au
serveur SMTP. Si celui-ci n'est pas fourni dans la configuration, il sera demandé
à l'envoi des emails.

#### Exemple avec Gmail

Pour une configuration d'envoi via Gmail, il faut au préalable
[autoriser les applications moins sécurisées](https://myaccount.google.com/lesssecureapps)
à s'exécuter sur son compte (option indisponible si l'authentification à 2 facteurs est activée),
puis la configuration SMTP sera la suivante :
```yaml
smtp:
  server: smtp.gmail.com
  port: 465
  login: <login gmail>@gmail.com
  password: <mot de passe du compte Google> # optionnel
```

### Local (enregistrement du résultat dans des fichiers)

Cet objet donne l'information du dossier où doivent être enregistrés les résultats
du tirage sur l'ordinateur exécutant le programme. Cette configuration se fait ainsi :
```yaml
local:
  destdir: # dossier de destination
```
Si cet objet est renseigné, le dossier donné comprendra à la fin de l'exécution un fichier
text par personne du tirage, chaque fichier ayant le nom d'une personne. Chacun de ces
fichiers contiendra l'email qui doit être envoyé à la personne dont le nom est le nom du fichier.
(`Bob.txt` contient le mail destiné à Bob)

**Note :** le dossier renseigné dans le champ `destdir` doit déjà exister au moment de l'exécution
pour que celle-ci se fasse !

## Exécution

Avant d'exécuter le programme, il est nécessaire d'installer les dépendances
Python sur son environnement en utilisant :
```shell
pip install -r requirements.txt
```
Le programme peut alors être lancé. Deux options sont possibles :
```shell
python3 main.py # si la configuration est dans data.yml
python3 main.py ./path-to-configuration.yml # si la configuration est ailleurs
```
