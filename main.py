import sys

from utils.data import read_file, validate_data
from utils.dest_list import build_dest_list
from utils.email import send_emails
from utils.local import save_local_result

if __name__ == '__main__':
    data = None
    if len(sys.argv) < 2:
        try:
            data = read_file('data.yml')
        except:
            print("ERROR: Missing program data")
            print("Usage: python3 main.py data.yml")
            exit()
    else:
        data = read_file(sys.argv[1])
    validate_data(data)

    dest_list = build_dest_list(data['people'])
    if 'local' in data:
        save_local_result(data, dest_list)
    if 'smtp' in data:
        send_emails(data, dest_list)
