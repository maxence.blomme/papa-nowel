﻿from random import choice


def try_build_dest_list(ppl_list):
    names = list(ppl_list)
    dest_list = {}
    first = names.pop()
    current = first
    while len(names) > 0:
        possibilities = \
            list(filter(lambda n: 'cantget' not in ppl_list[current] or n not in ppl_list[current]['cantget'], names))
        if len(possibilities) == 0:
            return None
        next_name = choice(possibilities)
        dest_list[current] = next_name
        current = names.pop(names.index(next_name))
    if 'cantget' in ppl_list[current] and first in ppl_list[current]['cantget']:
        return None
    dest_list[current] = first
    return dest_list


def build_dest_list(ppl_list):
    dest_list = None
    max_tries = 1000
    try_count = 0
    while dest_list is None and try_count < max_tries:
        dest_list = try_build_dest_list(ppl_list)
        try_count += 1
    if try_count >= max_tries:
        print('ERROR: Cannot create dest list that satisfies all criteria')
        exit()
    return dest_list
