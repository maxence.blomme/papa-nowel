﻿import yaml


def read_file(filename):
    try:
        with open(filename, 'r', encoding='utf-8') as stream:
            data = yaml.safe_load(stream)
    except:
        print('ERROR: Cannot open program data')
        print('Usage: python3 main.py data.yml')
        exit()
    return data


def validate_data(data):
    missing = []
    if 'smtp' not in data:
        print("WARNING: no SMTP config -> the messages won't be sent via email")
    else:
        if data['smtp'] is None or 'server' not in data['smtp']:
            missing.append('smtp.server')
        if data['smtp'] is None or 'port' not in data['smtp']:
            missing.append('smtp.port')
        if data['smtp'] is None or 'login' not in data['smtp']:
            missing.append('smtp.login')
    if 'local' not in data:
        print("WARNING: no local config -> the messages won't be stored in local files")
    else:
        if data['local'] is None or 'destdir' not in data['local']:
            missing.append('local.destdir')
    if 'email' not in data:
        missing.append('email')
    else:
        if data['email'] is None or 'subject' not in data['email']:
            missing.append('email.subject')
        if data['email'] is None or 'content' not in data['email']:
            missing.append('email.content')
    if 'people' not in data:
        missing.append('people')
    else:
        for person in data['people']:
            if data['people'][person] is None or 'email' not in data['people'][person]:
                missing.append('people.' + person + '.email')

    if len(missing) > 0:
        print('ERROR: Missing fields in data file')
        for miss in missing:
            print('  - ' + miss)
        exit()
