import os


def save_local_result(data, dst_list):
    email_data = data['email']
    ppl_list = data['people']
    for name in ppl_list:
        subject = email_data['subject'].format(name=name, dest=dst_list[name])
        content = email_data['content'].format(name=name, dest=dst_list[name])
        filename = os.path.join(data['local']['destdir'], name + '.txt')
        f = open(filename, 'w')
        f.write(subject + '\n\n' + content)
        f.close()
