import smtplib
import ssl
from email.message import EmailMessage


class Email:
    def __init__(self, to, subject, content):
        self.mail = EmailMessage()
        self.mail.set_content(content)
        self.mail['Subject'] = subject
        self.mail['To'] = to

    def send(self, data, server):
        server.sendmail(data['smtp']['login'], self.mail['To'], self.mail.as_string())


def connect_server(data):
    context = ssl.create_default_context()
    password = data['smtp']['password'] if 'password' in data['smtp'] else input("Type SMTP password and press enter: ")
    server = smtplib.SMTP_SSL(data['smtp']['server'], data['smtp']['port'], context=context)
    server.login(data['smtp']['login'], password)
    return server


def send_emails(data, dst_list):
    emails = []
    email_data = data['email']
    ppl_list = data['people']
    for name in ppl_list:
        subject = email_data['subject'].format(name=name, dest=dst_list[name])
        content = email_data['content'].format(name=name, dest=dst_list[name])
        emails.append(Email(ppl_list[name]['email'], subject, content))
    server = connect_server(data)
    for email in emails:
        email.send(data, server)
    server.close()
